<?php
declare(strict_types=1);

namespace AppBundle\Service;

use AppBundle\Entity\HealthCheck;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

/**
 *
 */
final class HealthCheckService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     *
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     *
     */
    public function getHealthCheck(): HealthCheck
    {
        $healthCheck = $this->entityManager
            ->getRepository(HealthCheck::class)
            ->matching(new Criteria(null, ['lastCheck' => 'DESC']))
            ->first()
        ;

        if ($healthCheck instanceof HealthCheck) {
            return $healthCheck;
        }

        return $this->doHealthCheck();
    }

    /**
     *
     */
    public function doHealthCheck(): HealthCheck
    {
        $healthCheck = new HealthCheck;
        $this->entityManager->persist($healthCheck);
        $this->entityManager->flush();

        return $healthCheck;
    }
}

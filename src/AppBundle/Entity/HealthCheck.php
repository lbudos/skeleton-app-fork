<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\Uuid;
use DateTime;

/**
 * @ORM\Table(name="health_check")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HealthCheckRepository")
 *
 * @Serializer\ExclusionPolicy("none")
 */
final class HealthCheck
{
    /**
     * @var Uuid
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     *
     * @Serializer\Type("string")
     */
    private $identity;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="lastCheck", type="datetime", nullable=false)
     *
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     * @Serializer\SerializedName("lastCheck")
     */
    private $lastCheck;

    /**
     *
     */
    public function __construct()
    {
        $this->lastCheck = new DateTime;
    }

    /**
     *
     */
    public function getIdentity(): Uuid
    {
        return $this->identity;
    }

    /**
     *
     */
    public function getLastCheck(): DateTime
    {
        return $this->lastCheck;
    }
}

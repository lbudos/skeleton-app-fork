<?php

namespace AppBundle\Controller;

use AppBundle\Service\HealthCheckService;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 */
final class DefaultController extends Controller
{
    /**
     * @var HealthCheckService
     */
    private $healthCheck;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     *
     */
    public function __construct(HealthCheckService $healthCheck, SerializerInterface $serializer)
    {
        $this->healthCheck = $healthCheck;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", name="homepage")
     *
     * @SuppressWarnings("PHPMD.UnusedFormalParameter")
     */
    public function indexAction(): Response
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/health-check", name="health_check")
     */
    public function healthCheckAction(): Response
    {
        return new Response(
            $this->serializer->serialize(
                $this->healthCheck->getHealthCheck(),
                'json'
            )
        );
    }
}
